import * as React from "react";
import { connect } from "react-redux";

import moment from "moment";

import { SearchAndSortBlock } from "../search-and-sort-block/"


import {
    setSearchIsFocusedValue,
    setSearchQuery,
    setSortingIsOpenValue,
    setSortingVariant,
    toggleInverse,
    setAvengersList
} from "../../app/actions/";

class AvengerSearchAndSortBlock extends React.Component {


    render() {

        const {
            isFocused,
            query,
            isOpen,
            sortVariants,
            inverse,
        } = this.props;

        return (
            <SearchAndSortBlock
                isFocus={isFocused}
                isOpen={isOpen}
                onFocus={e => this.onFocus(e)}
                onBlur={e => this.onBlur(e)}
                value={query}
                onChange={e => this.onChange(e)}
                onClickSorting={e => this.onClickSorting(e)}
                selectOptions={sortVariants}
                onSortArrowClick={e => this.onSortArrowClick(e)}
                onSelect={(e, option) => this.onSelect(e, option)}
                isInverse={!inverse}
                onKeyPress={e => this.onKeyPress(e)}
            />
        )
    }

    getLocalList(remoteList) {
        //еще один классный хак
        return remoteList.map(avenger => {
            const local = localStorage.getItem('avenger_'+avenger.id);

            if (local) {
                const parsed = JSON.parse(local);

                return parsed.removed
                    ? null
                    : Object.assign({}, avenger, parsed);
            }

            return avenger;
        }).filter(el => !!el);
    }

    onKeyPress(e) {
        if (e.key === 'Enter') {
            console.log('enter press');
            this.getAvengerListByQuery(e.target.value)
                .then(answer => answer.json())
                .then(answer => {

                    //здесь тупо копипаст
                    //в продакшене вынес бы в отдельный модуль в helpers или utils
                    console.log(answer);
                    const localList = this.getLocalList(answer);

                    const currentSortVariant = this.props.sortVariants.find(n => n.current);

                    console.log(currentSortVariant);

                    localList.sort((a, b) => {
                        switch (currentSortVariant.field) {
                            case 'birth_date':
                                return this.props.inverse
                                    ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                                    : -moment(a.birth_date).diff(b.birth_date, 'minutes');
                            case 'first_name':
                                return this.props.inverse
                                    ? a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase())
                                    : -a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase());
                            case 'rank':
                                return this.props.inverse
                                    ? a.post - b.post
                                    : b.post - a.post;
                            default:
                                return a - b;
                        }
                        return this.props.inverse
                            ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                            : -moment(a.birth_date).diff(b.birth_date, 'minutes');
                    });

                    this.props.setAvengersList(localList);

                })
        }
    }

    getAvengerListByQuery(q) {
        return fetch(
            `http://avengers.view.indev-group.eu/test_api/staff/?query=${q}`,
            {method: "GET"}
        );
    }

    onSortArrowClick(e) {
        this.props.toggleInverse();

        const currentSortVariant = this.props.sortVariants.find(n => n.current);

        const newList = this.props.avengerList
            .map(a => a)
            .sort((a, b) => {
                switch (currentSortVariant.field) {
                    case 'birth_date':
                        return !this.props.inverse
                            ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                            : -moment(a.birth_date).diff(b.birth_date, 'minutes');
                    case 'first_name':
                        return !this.props.inverse
                            ? a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase())
                            : -a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase());
                    case 'rank':
                        return !this.props.inverse
                            ? a.post - b.post
                            : b.post - a.post;
                    default:
                        return a - b;
                }
                return !this.props.inverse
                    ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                    : -moment(a.birth_date).diff(b.birth_date, 'minutes');
            });

        this.props.setAvengersList(newList);
    }

    onFocus(e) {
        this.props.setIsFocusedValue(true);
    }

    onBlur(e) {
        this.props.setIsFocusedValue(false);
    }

    onChange(e) {
        this.props.setSearchQuery(e.target.value);
    }

    onClickSorting(e) {
        this.props.setSortingIsOpenValue(!this.props.isOpen);
    }

    onSelect(e, option) {
        console.log('1', option);
        this.props.setSortingIsOpenValue(false);
        this.props.setSortingVariant(option);

        const newList = this.props.avengerList
            .map(a => a)
            .sort((a, b) => {
            switch (option.field) {
                case 'birth_date':
                    return this.props.inverse
                        ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                        : -moment(a.birth_date).diff(b.birth_date, 'minutes');
                case 'first_name':
                    return this.props.inverse
                        ? a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase())
                        : -a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase());
                case 'rank':
                    return this.props.inverse
                        ? a.post - b.post
                        : b.post - a.post;
                default:
                    return a - b;
            }
            return this.props.inverse
                ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                : -moment(a.birth_date).diff(b.birth_date, 'minutes');
        });

        this.props.setAvengersList(newList);
    }
}

const mapStateToProps = (state, props) => {

    const newProps = {
        isFocused: state.AvengerSearchAndSortBlock.isFocused,
        query: state.AvengerSearchAndSortBlock.query,
        isOpen: state.AvengerSearchAndSortBlock.isOpen,
        sortVariants: state.AvengerSearchAndSortBlock.sortVariants,
        inverse: state.AvengerSearchAndSortBlock.inverse,
        avengerList: state.AvengerList.avengerList,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    setIsFocusedValue: val => dispatch(setSearchIsFocusedValue(val)),
    setSearchQuery:    val => dispatch(setSearchQuery(val)),
    setSortingIsOpenValue: val => dispatch(setSortingIsOpenValue(val)),
    setSortingVariant: val => dispatch(setSortingVariant(val)),
    toggleInverse: val => dispatch(toggleInverse()),
    setAvengersList: val => dispatch(setAvengersList(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AvengerSearchAndSortBlock);