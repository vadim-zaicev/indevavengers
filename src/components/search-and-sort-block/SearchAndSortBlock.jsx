import * as React from "react";

import { SearchIcon } from "./__search-icon/SearchIcon"
import { InputBlock } from "../input-block/"

class SearchAndSortBlock extends React.Component {


    render() {

        const {
            isOpen,
            isFocus,
            selectOptions,
            isInverse,
            onSortArrowClick,
            onSelect,
            onBlur,
            onFocus,
            onChange,
            onKeyPress,
            onClickSorting,
            value
        } = this.props;

        const className = "search-and-sort-block"
            + (isFocus
                ? " search-and-sort-block_focus"
                : "");

        const arrowClassName = "search-and-sort-block__sort-arrow"
            + (isInverse
                ? " search-and-sort-block__sort-arrow_inverse"
                : "");

        const currentOption = selectOptions && selectOptions.length
            ? selectOptions.find(op => op.current)
            : null;


        return (
            <div className={className}>

                <SearchIcon/>

                <input
                    type="text"
                    className="search-and-sort-block__search-input"
                    placeholder="Поиск"
                    value={value}
                    onChange={onChange ? e => onChange(e) : null}
                    onFocus={onFocus ? e => onFocus(e) : null}
                    onBlur={onBlur ? e => onBlur(e) : null}
                    onKeyPress={onKeyPress ? e => onKeyPress(e) : null}
                />

                <div className="search-and-sort-block__sort-container">

                    <InputBlock
                        blockType="select"
                        isWithCheckMark={true}
                        value={currentOption && currentOption.title ? currentOption.title : ''}
                        isOpen={isOpen}
                        onSelect={onSelect ? (e, option) => onSelect(e, option) : null}
                        selectOptions={selectOptions}
                        onClick={e => onClickSorting(e)}
                    >
                        <div
                            className={arrowClassName}
                            onClick={onSortArrowClick ? e => onSortArrowClick(e) : null}
                        ></div>
                    </InputBlock>

                </div>
            </div>
        )
    }
}

export { SearchAndSortBlock };