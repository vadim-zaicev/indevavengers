import * as React from "react";
import { connect } from "react-redux";
import { createSelector } from 'reselect';

import moment from "moment";

import { InputBlock } from "../../components/input-block/"

import { setBirtdateIsFocusedValue, setBirthdate } from "../../app/actions/";
import { getDateString, getDateForInput } from "../../app/selectors/";

class BirthdateSelectBlock extends React.Component {

    render() {

        const {
            isFocused,
            birthdate
        } = this.props;

        const isEmpty = isFocused
            ? false
            : (birthdate.length ? false : true);

        const value = getDateString(birthdate);

        console.log('value update', value, 'birthdate', birthdate);

        return (
            <InputBlock
                title="Дата рождения"
                blockType="select"
                isEmpty={isEmpty}
                isFocus={isFocused}
                onChange={date => this.onChange(date)}
                onFocus={e => this.onFocus(e)}
                onBlur={e => this.onBlur(e)}
                isDatePicker={true}
                startDate={birthdate
                    ? getDateForInput(birthdate)
                    : null}
                value={value}
                dateFormat="DD MMMM, YYYY"
            />
        )
    }

    onChange(date) {
        this.props.setBirthdate(moment(date).format());
    }

    onFocus(e) {
        this.props.setIsFocusedValue(true);
    }

    onBlur(e) {
        this.props.setIsFocusedValue(false);
    }

}

const mapStateToProps = (state, props) => {

    const newProps = {
        isFocused: state.BirthdateSelectBlock.isFocused,
        birthdate: state.BirthdateSelectBlock.birthdate,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    setIsFocusedValue: val => dispatch(setBirtdateIsFocusedValue(val)),
    setBirthdate:      val => dispatch(setBirthdate(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BirthdateSelectBlock);