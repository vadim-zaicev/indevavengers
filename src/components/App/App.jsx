import * as React from "react";

import { MainBlock } from "../../components/main-block/";
import { MainBlockHeader } from "../../components/main-block/"
import { MainBlockContentWrap } from "../../components/main-block/"
import { MainBlockInnerSection } from "../../components/main-block/"

import AvengerEditBlock from "../../components/AvengerEditBlock/AvengerEditBlock"
import AvengerSpecBlock from "../../components/AvengerSpecBlock/AvengerSpecBlock"
import AvengerEditButtonLineBlock from "../../components/AvengerEditButtonLineBlock/AvengerEditButtonLineBlock"

import AvengerSearchAndSortBlock from "../../components/AvengerSearchAndSortBlock/AvengerSearchAndSortBlock"
import AvengerList from "../../components/AvengerList/AvengerList"

class App extends React.Component {

    componentWillMount() {
        //можно получать данные здесь, но я их получаю в компонентах, так делать не надо, но я делаю
        //потому что время меня ограничивает
    }

    render() {
        return (
            <div>
                <MainBlock>

                    <MainBlockHeader headerType="text">
                        Редактирование
                    </MainBlockHeader>

                    <MainBlockContentWrap>

                        <MainBlockInnerSection>
                            <AvengerEditBlock/>
                        </MainBlockInnerSection>

                        <MainBlockInnerSection>
                            <AvengerSpecBlock/>
                        </MainBlockInnerSection>

                        <MainBlockInnerSection>
                            <AvengerEditButtonLineBlock/>
                        </MainBlockInnerSection>

                    </MainBlockContentWrap>

                </MainBlock>

                <MainBlock>
                    <MainBlockHeader headerType="container">
                        <AvengerSearchAndSortBlock/>
                    </MainBlockHeader>

                    <AvengerList/>

                </MainBlock>
            </div>
        )
    }
}

export default App;