import * as React from "react";

export const TextareaBlock = props => {

    const {
        id,
        title,
        children,
        isEmpty,
        isFocus,
        ...otherProps
    } = props;

    const className = "textarea-block"

        + (isEmpty
            ? " textarea-block_empty"
            : "")

        + (isFocus
            ? " textarea-block_focus"
            : "");

    return (
        <div className={className}>
            <label
                htmlFor={id}
                className="textarea-block__label">{title}</label>
            <textarea
                className="textarea-block__textarea"
                id={id}
                {...otherProps}
            >
            </textarea>
        </div>
    )
};