import * as React from "react";
import { connect } from "react-redux";

import { AvengerEditBlockContainer } from "../../components/avenger-edit-block/"
import { AvengerEditBlockImageWrapContainer } from "../../components/avenger-edit-block/"
import { AvengerEditBlockInputsWrapContainer } from "../../components/avenger-edit-block/"
import { AvengerEditBlockImage } from "../../components/avenger-edit-block/"

import SurnameInputBlock from "../../components/SurnameInputBlock/SurnameInputBlock"
import NameInputBlock from "../../components/NameInputBlock/NameInputBlock"
import BirthdateSelectBlock from "../../components/BirthdateSelectBlock/BirthdateSelectBlock"
import RankSelectBlock from "../../components/RankSelectBlock/RankSelectBlock"

class AvengerEditBlock extends React.Component {

    render() {

        return (
            <AvengerEditBlockContainer>

                <AvengerEditBlockImageWrapContainer>
                    <AvengerEditBlockImage
                        image={this.props.image}
                    />
                </AvengerEditBlockImageWrapContainer>

                <AvengerEditBlockInputsWrapContainer>
                    <SurnameInputBlock/>
                    <NameInputBlock/>
                    <BirthdateSelectBlock/>
                    <RankSelectBlock/>
                </AvengerEditBlockInputsWrapContainer>

            </AvengerEditBlockContainer>
        )
    }
}

const mapStateToProps = (state, props) => {

    const newProps = {
        image: state.AvengerEditBlock.image
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(AvengerEditBlock);