import * as React from "react";
import { connect } from "react-redux";

import { ButtonLine } from "../button-line/";

import {
    saveCurrentAvenger,
    removeAvenger,
    setCurrentAvenger
} from "../../app/actions/";

import { StoreHelper } from "../../app/helpers/StoreHelper";

class AvengerEditButtonLineBlock extends React.Component {

    render() {

        return (
            <ButtonLine>
                <button className="button button_size_small" onClick={e => this.onSave(e)}>Сохранить</button>
                <button className="button button_size_small button_color_gray" onClick={e => this.onRemove(e)}>Удалить</button>
            </ButtonLine>
        );
    }

    onSave(e) {
        if (!this.props.name.length) return;

        this.props.saveCurrentAvenger({
            id: this.props.currentId,
            first_name: this.props.name,
            last_name: this.props.surname,
            birth_date: this.props.birthdate,
            description: this.props.spec,
            rank: this.props.rank.id
        });

        this.updateLocal({
            id: this.props.currentId,
            first_name: this.props.name,
            last_name: this.props.surname,
            birth_date: this.props.birthdate,
            description: this.props.spec,
            rank: this.props.rank.id
        });
    }

    onRemove(e) {
        this.updateLocal({
            id: this.props.currentId,
            first_name: this.props.name,
            last_name: this.props.surname,
            birth_date: this.props.birthdate,
            description: this.props.spec,
            rank: this.props.rank.id,
            removed: true
        }); //это в отдельную сущность типа LocalHelper, либо в serviceWorker'а

        this.props.removeAvenger({
                id: this.props.currentId,
                first_name: this.props.name,
                last_name: this.props.surname,
                birth_date: this.props.birthdate,
                description: this.props.spec,
                rank: this.props.rank.id,
                removed: true
            });

        //Вот я и столкнулся с тем, что мне нужна прямая ссылка на Store, а не this.props

        this.props.setCurrentAvenger(StoreHelper.getState().AvengerList.avengerList.find(n => true));

    }

    updateLocal(avenger) {
        localStorage.setItem('avenger_'+avenger.id, JSON.stringify(avenger));
    }

}

const mapStateToProps = (state, props) => {

    const newProps = {
        currentId: state.AvengerEditBlock.currentId,
        name: state.NameInputBlock.name,
        surname: state.SurnameInputBlock.surname,
        rank: state.RankSelectBlock.rank,
        birthdate: state.BirthdateSelectBlock.birthdate,
        spec: state.AvengerSpecBlock.spec,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    saveCurrentAvenger: val => dispatch(saveCurrentAvenger(val)),
    removeAvenger:      val => dispatch(removeAvenger(val)),
    setCurrentAvenger:  val => dispatch(setCurrentAvenger(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AvengerEditButtonLineBlock);