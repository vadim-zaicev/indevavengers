import * as React from "react";

import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

class InputBlock extends React.Component {

    renderSelectVariant(option, i = -1) {
        const className = "input-block__select-variant"
            + (option.current
                ? " input-block__select-variant_current"
                : "");

        return (<div className={className} key={i} onClick={e => this.onSelect(e, option)}>{option.title}</div>)
    }

    render() {

        const {
            title,
            id,
            isError,
            isEmpty,
            isFocus,
            isOpen,
            blockType,
            isWithCheckMark,
            selectOptions,
            children,
            isDatePicker,
            startDate,
            onSelect,
            ...otherProps
        } = this.props;

        this.onSelect = onSelect;

        const className = "input-block"

            + (isEmpty
                ? " input-block_empty"
                : "")

            + (isFocus
                ? " input-block_focus"
                : "")

            + (isOpen
                ? " input-block_open"
                : "")

            + (isError
                ? " input-block_error"
                : "")

            + (blockType
                ? ` input-block_type_${blockType}`
                : "")

            + (isWithCheckMark
                ? " input-block_type_select_with-checkmark"
                : "");



            const selectVariants = selectOptions
                ? (
                <div className="input-block__select-variants-wrap">
                    {selectOptions.map((v, i) => this.renderSelectVariant(v, i))}
                </div>
                )
                : null;

            const errorInfo = isError
                ? (<label htmlFor={id} className="input-block__error-label">! Поле не заполнено</label>)
                : null;

            const input = isDatePicker

                ? (<DatePicker
                        selected={startDate}
                        id={id}
                        {...otherProps}
                        className="input-block__input"
                        locale="ru"
                    />)

                : (<input className="input-block__input"
                       type="text"
                       id={id}
                       {...otherProps}
                />);


        return (
            <div className={className}>
                <label htmlFor={id} className="input-block__label">{title}</label>

                {input}

                {errorInfo}

                {selectVariants}

                {children}

            </div>
        )
    }
}

export { InputBlock };