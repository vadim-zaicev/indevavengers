import * as React from "react";

class AvengerInfoBlock extends React.Component {

    render() {

        const {
            avenger,
            onEdit
        } = this.props;

        return (
            <div className="avenger-info-block">

                <div className="avenger-info-block__image-wrap">
                    <div className="avenger-info-block__image" style={{backgroundImage: `url('${avenger.image}')`}}>
                    </div>
                </div>

                <div className="avenger-info-block__info-wrap">

                    <div className="avenger-info-block__name">
                        {avenger.first_name} {avenger.last_name}
                    </div>
                    <div className="avenger-info-block__birthdate">
                        {avenger.birth_date_formatted}
                    </div>
                    <div className="avenger-info-block__rank">
                        {avenger.post_name}
                    </div>

                    <div className="button-line">
                        <button className="button button_size_medium" onClick={onEdit ? e => onEdit(e) : null}>Редактировать</button>
                    </div>

                </div>

            </div>
        )
    }
}

export { AvengerInfoBlock };