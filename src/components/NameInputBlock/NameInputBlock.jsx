import * as React from "react";
import { connect } from "react-redux";

import {
    setName,
    setNameIsFocusedValue,
} from "../../app/actions/";

import { InputBlock } from "../../components/input-block/"

class NameInputBlock extends React.Component {

    render() {

        const {
            isFocused,
        } = this.props;

        const isEmpty = isFocused
            ? false
            : (this.props.name.length ? false : true);

        const isError = this.props.name.length ? false : true;

        return (
            <InputBlock
                isEmpty={isEmpty}
                isFocus={isFocused}
                isError={isError}
                title="Имя"
                value={this.props.name}
                onChange={e => this.onChange(e)}
                onFocus={e => this.onFocus(e)}
                onBlur={e => this.onBlur(e)}
            />
        )
    }

    onChange(e) {
        this.props.setName(e.target.value);
    }

    onFocus(e) {
        this.props.setIsFocusedValue(true);
    }

    onBlur(e) {
        this.props.setIsFocusedValue(false);
    }

}

const mapStateToProps = (state, props) => {

    const newProps = {
        name: state.NameInputBlock.name,
        isFocused: state.NameInputBlock.isFocused,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    setName:           val => dispatch(setName(val)),
    setIsFocusedValue: val => dispatch(setNameIsFocusedValue(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NameInputBlock);