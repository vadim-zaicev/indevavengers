import * as React from "react";
import { connect } from "react-redux";

import moment from "moment";

import { AvengerInfoBlock } from "../avenger-info-block/"

import { MainBlockContentWrap } from "../main-block/"

import {
    setAvengersList,
    setCurrentAvenger
} from "../../app/actions/";

import {
    getDateString
} from "../../app/selectors/";

class AvengerList extends React.Component {

    //Все методы запросов надо выносить в отдельную сущность Service (AvengerService)
    //Но я не спал ночь, поэтому этот boilerplate в данном случае (компонент-то это делает один)
    //Делать не буду

    getAvengerList() {
        return fetch(
            "http://avengers.view.indev-group.eu/test_api/staff/",
            {method: "GET"}
        );
    }

    getLocalList(remoteList) {
        //еще один классный хак
        return remoteList.map(avenger => {
           const local = localStorage.getItem('avenger_'+avenger.id);

           if (local) {
                const parsed = JSON.parse(local);

                return parsed.removed
                    ? null
                    : Object.assign({}, avenger, parsed);
           }

           return avenger;
        }).filter(el => !!el);
    }

    componentWillMount() {
        this.getAvengerList()
            .then(answer => answer.json())
            .then(answer => {
                const localList = this.getLocalList(answer);

                const currentSortVariant = this.props.sortVariants.find(n => n.current);

                console.log(currentSortVariant);

                localList.sort((a, b) => {
                    switch (currentSortVariant.field) {
                        case 'birth_date':
                            return this.props.inverse
                                ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                                : -moment(a.birth_date).diff(b.birth_date, 'minutes');
                        case 'first_name':
                            return this.props.inverse
                                ? a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase())
                                : -a.first_name.toLowerCase().localeCompare(b.first_name.toLowerCase());
                        case 'rank':
                            return this.props.inverse
                                ? a.post - b.post
                                : b.post - a.post;
                        default:
                            return a - b;
                    }
                    return this.props.inverse
                        ? moment(a.birth_date).diff(b.birth_date, 'minutes')
                        : -moment(a.birth_date).diff(b.birth_date, 'minutes');
                });

                this.props.setAvengersList(localList);
                this.props.setCurrentAvenger(localList.find(n => true));
            });
    }

    render() {

        return (
            <MainBlockContentWrap className="avengers-list-container">
                {this.props.avengerList.map((avenger, i) => this.renderAvenger(avenger, i))}
            </MainBlockContentWrap>
        )
    }

    renderAvenger(avenger, i = -1) {

        const rank = this.props.rankList.find(r => r.id == avenger.post);

        return (
            <AvengerInfoBlock
                key={i}
                avenger={
                    Object.assign({}, avenger, {
                        birth_date_formatted: getDateString(avenger.birth_date),
                        post_name: rank && rank.name ? rank.name : ''
                    })
                }
                onEdit={e => this.onEdit(e, avenger)}
            />
        )
    }

    onEdit(e, avenger) {
        console.log('edit', avenger);
        this.props.setCurrentAvenger(avenger);

        //здесь грубый хак, но не охота писать подсистему, которая будет возвращать рефы, проще обратиться к DOM

        const avengerEditBlock = document.querySelector('.avenger-edit-block');

        if (avengerEditBlock) {
            avengerEditBlock.scrollIntoView();
        }

    }

}

const mapStateToProps = (state, props) => {

    const newProps = {
        avengerList: state.AvengerList.avengerList,
        sortVariants: state.AvengerSearchAndSortBlock.sortVariants,
        inverse: state.AvengerSearchAndSortBlock.inverse,
        rankList: state.RankSelectBlock.rankList,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    setAvengersList: val => dispatch(setAvengersList(val)),
    setCurrentAvenger: val => dispatch(setCurrentAvenger(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AvengerList);