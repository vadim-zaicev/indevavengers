import * as React from "react";
import { connect } from "react-redux";
import { createSelector } from 'reselect';

import { InputBlock } from "../../components/input-block/"

import {
    setRankIsFocusedValue,
    setRankIsOpenValue,
    setRank,
    setRankList,
} from "../../app/actions/";

class RankSelectBlock extends React.Component {

    openTimeout;

    getRankList() {
        return fetch(
            "http://avengers.view.indev-group.eu/test_api/posts/",
            {method: "GET"}
        );
    }

    //Надо вынести в сервисы: RankService

    componentWillMount() {
        this.getRankList()
            .then(answer => answer.json())
            .then(answer => this.props.setRankList(answer));
    }

    render() {

        const {
            isFocused,
            isOpen,
            rank,
            rankList
        } = this.props;

        const isEmpty = isFocused
            ? false
            : (rank.id ? false : true);



        return (
            <InputBlock
                title="Звание"
                blockType="select"
                isEmpty={isEmpty}
                isOpen={isOpen}
                isFocus={isFocused}
                selectOptions={rankList.map(r => {
                    r.title = r.name;
                    return r;
                })}
                onChange={e => this.onChange(e)}
                onFocus={e => this.onFocus(e)}
                onClick={e => this.onClick(e)}
                onBlur={e => this.onBlur(e)}
                value={rank.id ? rankList.find(r => r.id == rank.id).name : ''}
                onSelect={(e, option) => this.onSelect(e, option)}
            />
        )
    }

    onClick(e) {
        this.props.setIsOpenValue(!this.props.isOpen);
    }

    onSelect(e, option) {
        console.log(option);
        this.props.setRank(option);
        this.props.setIsOpenValue(false);
    }

    onChange(e) {
        console.log(e.target.value);
    }

    onFocus(e) {
        this.props.setIsFocusedValue(true);
    }

    onBlur(e) {
        this.props.setIsFocusedValue(false);
    }

}

const mapStateToProps = (state, props) => {

    const newProps = {
        isFocused: state.RankSelectBlock.isFocused,
        isOpen: state.RankSelectBlock.isOpen,
        rank: state.RankSelectBlock.rank,
        rankList: state.RankSelectBlock.rankList,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    setIsFocusedValue: val => dispatch(setRankIsFocusedValue(val)),
    setIsOpenValue:    val => dispatch(setRankIsOpenValue(val)),
    setRank:           val => dispatch(setRank(val)),
    setRankList:       val => dispatch(setRankList(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RankSelectBlock);