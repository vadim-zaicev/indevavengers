import * as React from "react";

export const ButtonLine = props => {

    return (
        <div className="button-line">
            {props.children}
        </div>
    );
};