import * as React from "react";
import { connect } from "react-redux";

import {
    setSurname,
    setSurnameIsFocusedValue
} from "../../app/actions/";

import { InputBlock } from "../../components/input-block/"

class SurnameInputBlock extends React.Component {

    render() {

        const {
            surname,
            isFocused
        } = this.props;

        const isEmpty = isFocused
            ? false
            : (surname.length ? false : true);

        return (
            <InputBlock
                isEmpty={isEmpty}
                isFocus={isFocused}
                title="Фамилия"
                value={surname}
                onChange={e => this.onChange(e)}
                onFocus={e => this.onFocus(e)}
                onBlur={e => this.onBlur(e)}
            />
        )
    }

    onChange(e) {
        this.props.setSurname(e.target.value);
    }

    onFocus(e) {
        this.props.setIsFocusedValue(true);
    }

    onBlur(e) {
        this.props.setIsFocusedValue(false);
    }

}

const mapStateToProps = (state, props) => {

    const newProps = {
        surname: state.SurnameInputBlock.surname,
        isFocused: state.SurnameInputBlock.isFocused,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    setSurname:        val => dispatch(setSurname(val)),
    setIsFocusedValue: val => dispatch(setSurnameIsFocusedValue(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SurnameInputBlock);