import * as React from "react";
import { connect } from "react-redux";

import { TextareaBlock } from "../../components/textarea-block/"

import {
    setSpec,
    setSpecIsFocusedValue
} from "../../app/actions/";

class AvengerSpecBlock extends React.Component {

    render() {

        const {
            isFocused,
            spec,
        } = this.props;

        const isEmpty = isFocused
            ? false
            : (spec.length ? false : true);

        return (
            <TextareaBlock
                title="Характеристика"
                value={spec}
                isFocus={isFocused}
                isEmpty={isEmpty}
                onFocus={e => this.onFocus(e)}
                onChange={e => this.onChange(e)}
                onBlur={e => this.onBlur(e)}
            />
        );
    }

    onFocus(e) {
        this.props.setIsFocusedValue(true);
    }

    onBlur(e) {
        this.props.setIsFocusedValue(false);
    }

    onChange(e) {
        this.props.setSpec(e.target.value);
    }
}

const mapStateToProps = (state, props) => {

    const newProps = {
        isFocused: state.AvengerSpecBlock.isFocused,
        spec: state.AvengerSpecBlock.spec,
    };

    return newProps;
};

const mapDispatchToProps = (dispatch) => ({
    setSpec:           val => dispatch(setSpec(val)),
    setIsFocusedValue: val => dispatch(setSpecIsFocusedValue(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AvengerSpecBlock);