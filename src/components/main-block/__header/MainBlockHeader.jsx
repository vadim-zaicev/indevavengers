import * as React from "react";

export const MainBlockHeader = props => {

    const className = "main-block__header"
        + (props.headerType
            ? ` main-block__header_type_${props.headerType}`
            : "");

    return (
        <header className={className}>
            {props.children}
        </header>
    )
};