import * as React from "react";

export const MainBlockContentWrap = props => {

    const className = "main-block__content-wrap"
        + (props.className
            ? " " + props.className
            : "");

    return (
        <section className={className}>
            {props.children}
        </section>
    )
};