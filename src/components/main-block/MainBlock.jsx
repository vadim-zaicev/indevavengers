import * as React from "react";

export const MainBlock = props => {
  return (
      <div className="main-block">
          {props.children}
      </div>
  )
};