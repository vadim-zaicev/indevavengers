export { MainBlock } from "./MainBlock";
export { MainBlockHeader } from "./__header/MainBlockHeader";
export { MainBlockContentWrap } from "./__content-wrap/MainBlockContentWrap";
export { MainBlockInnerSection } from "./__inner-section/MainBlockInnerSection";