import * as React from "react";

export const MainBlockInnerSection = props => {

    const className = "main-block__inner-section"
        + (props.className
            ? " " + props.className
            : "");

    return (
        <section className={className}>
            {props.children}
        </section>
    )
};