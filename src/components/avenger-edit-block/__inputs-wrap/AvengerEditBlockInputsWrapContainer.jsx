import * as React from "react";

export const AvengerEditBlockInputsWrapContainer = props => {
    return (
        <div className="avenger-edit-block__inputs-wrap">
            {props.children}
        </div>
    )
};