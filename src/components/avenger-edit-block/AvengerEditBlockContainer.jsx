import * as React from "react";

export const AvengerEditBlockContainer = props => {
    return (
        <div className="avenger-edit-block">
            {props.children}
        </div>
    )
};