import * as React from "react";

export const AvengerEditBlockImage = props => {
    return (
        <div className="avenger-edit-block__image" style={{backgroundImage: `url('${props.image}')`}}></div>
    )
};