import * as React from "react";

export const AvengerEditBlockImageWrapContainer = props => {
    return (
        <div className="avenger-edit-block__image-wrap">
            {props.children}
        </div>
    )
};