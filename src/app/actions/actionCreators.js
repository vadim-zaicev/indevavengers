import { constants } from "./constants";

export function setSurname(value) {
    return {
        type: constants.SURNAME_INPUT_BLOCK.SET_SURNAME,
        value
    };
}

export function setSurnameIsFocusedValue(val) {
    return {
        type: val
            ? constants.SURNAME_INPUT_BLOCK.SET_FOCUSED
            : constants.SURNAME_INPUT_BLOCK.SET_UNFOCUSED
    };
}

export function setName(value) {
    return {
        type: constants.NAME_INPUT_BLOCK.SET_NAME,
        value
    };
}

export function setNameIsFocusedValue(val) {
    return {
        type: val
            ? constants.NAME_INPUT_BLOCK.SET_FOCUSED
            : constants.NAME_INPUT_BLOCK.SET_UNFOCUSED
    };
}


export function setImage(value) {
    return {
        type: constants.AVENGER_EDIT_BLOCK.SET_IMAGE,
        value
    };
}

export function setBirtdateIsFocusedValue(val) {
    return {
        type: val
            ? constants.BIRTHDATE_SELECT_BLOCK.SET_FOCUSED
            : constants.BIRTHDATE_SELECT_BLOCK.SET_UNFOCUSED
    };
}

export function setRankIsFocusedValue(val) {
    return {
        type: val
            ? constants.RANK_SELECT_BLOCK.SET_FOCUSED
            : constants.RANK_SELECT_BLOCK.SET_UNFOCUSED
    };
}

export function setRank(value) {
    return {
        type: constants.RANK_SELECT_BLOCK.SET_RANK,
        value
    };
}

export function setRankIsOpenValue(val) {
    return {
        type: val
            ? constants.RANK_SELECT_BLOCK.SET_OPEN
            : constants.RANK_SELECT_BLOCK.SET_CLOSE
    };
}

export function setSpec(value) {
    return {
        type: constants.AVENGER_SPEC_BLOCK.SET_SPEC,
        value
    };
}

export function setSpecIsFocusedValue(val) {
    return {
        type: val
            ? constants.AVENGER_SPEC_BLOCK.SET_FOCUSED
            : constants.AVENGER_SPEC_BLOCK.SET_UNFOCUSED
    }
}

export function setSearchIsFocusedValue(val) {
    return {
        type: val
            ? constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_FOCUSED
            : constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_UNFOCUSED
    };
}

export function setSearchQuery(value) {
    return {
        type: constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_QUERY,
        value
    };
}

export function setSortingIsOpenValue(val) {
    return {
        type: val
            ? constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_OPEN
            : constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_CLOSE
    };
}

export function setSortingVariant(value) {
    return {
        type: constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_SORTING_VARIANT,
        value
    };
}

export function toggleInverse() {
    return {
        type: constants.AVENGER_SEARCH_AND_SORT_BLOCK.TOGGLE_INVERSE,
    };
}

export function setAvengersList(value) {
    return {
        type: constants.AVENGER_LIST.SET_LIST,
        value
    };
}

export function setRankList(value) {
    return {
        type: constants.RANK_SELECT_BLOCK.SET_RANK_LIST,
        value
    };
}

export function setCurrentAvenger(value) {
    console.log('set current avenger', value);
    return {
        type: constants.AVENGER_LIST.SET_CURRENT_AVENGER,
        value
    }
}

export function setBirthdate(value) {
    return {
        type: constants.BIRTHDATE_SELECT_BLOCK.SET_BIRTHDATE,
        value
    }
}

export function saveCurrentAvenger(value) {
    return {
        type: constants.AVENGER_EDIT_BUTTON_LINE.SAVE_CURRENT_AVENGER,
        value
    }
}

export function removeAvenger(value) {
    return {
        type: constants.AVENGER_EDIT_BUTTON_LINE.DELETE_CURRENT_AVENGER,
        value
    }
}