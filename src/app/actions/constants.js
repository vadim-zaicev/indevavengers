const constants = {
    SURNAME_INPUT_BLOCK: {
        SET_SURNAME: null,
        SET_FOCUSED: null,
        SET_UNFOCUSED: null,
    },
    NAME_INPUT_BLOCK: {
        SET_NAME: null,
        SET_FOCUSED: null,
        SET_UNFOCUSED: null,
        SET_INVALID: null,
        SET_VALID: null,
    },
    AVENGER_EDIT_BLOCK: {
        SET_IMAGE: null,
    },
    AVENGER_EDIT_BUTTON_LINE: {
        SAVE_CURRENT_AVENGER: null,
        DELETE_CURRENT_AVENGER: null,
    }
    ,
    BIRTHDATE_SELECT_BLOCK: {
        SET_BIRTHDATE: null,
        SET_FOCUSED: null,
        SET_UNFOCUSED: null,
    },
    RANK_SELECT_BLOCK: {
        SET_RANK: null,
        SET_FOCUSED: null,
        SET_UNFOCUSED: null,
        SET_OPEN: null,
        SET_CLOSE: null,
        SET_RANK_LIST: null,
    },
    AVENGER_SPEC_BLOCK: {
        SET_SPEC: null,
        SET_FOCUSED: null,
        SET_UNFOCUSED: null,
    },
    AVENGER_SEARCH_AND_SORT_BLOCK: {
        SET_QUERY: null,
        SET_FOCUSED: null,
        SET_UNFOCUSED: null,
        SET_OPEN: null,
        SET_CLOSE: null,
        SET_SORTING_VARIANT: null,
        TOGGLE_INVERSE: null,
        SET_SORT_VARIANTS: null,
    },
    AVENGER_LIST: {
        UPDATE_LIST_ITEM: null,
        SET_LIST: null,
        SET_CURRENT_AVENGER: null,
    }
};

function updateKey(obj, key, value) {

    if (obj[key] === null) {
        return value;
    }

    if (typeof obj[key] === 'string') {
        return obj[key];
    }

    const answer = {};

    for (const innerKey in obj[key]) {
        answer[innerKey] = updateKey(obj[key], innerKey, value + '_' + innerKey);
    }

    return answer;

}

for (const key in constants) {
    constants[key] = updateKey(constants, key, key);
}

export { constants };