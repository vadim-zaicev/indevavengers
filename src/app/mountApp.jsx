import * as React from "react";
import * as ReactDOM from "react-dom";
import {createStore} from "redux";
import {Provider} from "react-redux";
import reducer from "./reducers";
import App from "../components/App/App";

import { StoreHelper } from "./helpers/StoreHelper";


const mountApp = function () {
    console.log('mount app...');

    const store = createStore(reducer);
    StoreHelper.setStore(store);

    window['store'] = store; //для отладки пусть торчит наружу :D по хорошему, надо сделать if (PRODUCTION)...

    const container = document.querySelector('#react-app-container');

    //еще можно здесь начать получать данные...

    if (container) {
        ReactDOM.render(
            <Provider store={store}>
                <App />
            </Provider>,
            container
        );
    }

};

export { mountApp };