import moment from 'moment';

moment.locale('ru');

export function getDateString(date) {
    return moment(date).format("DD MMMM, YYYY");
}

export function getDateForInput(date) {
    return moment(date);
}