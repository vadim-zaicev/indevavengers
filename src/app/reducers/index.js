import { combineReducers } from 'redux';
import SurnameInputBlock from "./SurnameInputBlock";
import NameInputBlock from "./NameInputBlock";
import AvengerEditBlock from "./AvengerEditBlock";
import BirthdateSelectBlock from "./BirthdateSelectBlock";
import RankSelectBlock from "./RankSelectBlock";
import AvengerSpecBlock from "./AvengerSpecBlock";
import AvengerSearchAndSortBlock from "./AvengerSearchAndSortBlock";
import AvengerList from "./AvengerList";

export default combineReducers({
    SurnameInputBlock,
    NameInputBlock,
    AvengerEditBlock,
    BirthdateSelectBlock,
    RankSelectBlock,
    AvengerSpecBlock,
    AvengerSearchAndSortBlock,
    AvengerList,
});