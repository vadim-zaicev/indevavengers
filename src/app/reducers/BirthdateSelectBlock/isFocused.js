import { constants } from "../../actions/";

const isFocused = function(state = false, action) {
    switch (action.type) {
        case constants.BIRTHDATE_SELECT_BLOCK.SET_FOCUSED:
            return true;
        case constants.BIRTHDATE_SELECT_BLOCK.SET_UNFOCUSED:
            return false;
        default:
            return state;
    }
};

export default isFocused;