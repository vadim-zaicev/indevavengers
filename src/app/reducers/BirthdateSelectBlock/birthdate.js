import { constants } from "../../actions/";

const birthdate = function(state = '', action) {
    switch (action.type) {
        case constants.BIRTHDATE_SELECT_BLOCK.SET_BIRTHDATE:
            return action.value;
        case constants.AVENGER_LIST.SET_CURRENT_AVENGER:
            return action.value.birth_date;
        default:
            return state;
    }
};

export default birthdate;