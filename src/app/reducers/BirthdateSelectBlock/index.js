import { combineReducers } from 'redux';
import isFocused from "./isFocused";
import birthdate from "./birthdate";

export default combineReducers({
    isFocused,
    birthdate,
});