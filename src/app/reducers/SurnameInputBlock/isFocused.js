import { constants } from "../../actions/";

const isFocused = function(state = false, action) {
    switch (action.type) {
        case constants.SURNAME_INPUT_BLOCK.SET_FOCUSED:
            return true;
        case constants.SURNAME_INPUT_BLOCK.SET_UNFOCUSED:
            return false;
        default:
            return state;
    }
};

export default isFocused;