import { constants } from "../../actions/";

const surname = function(state = '', action) {
    switch (action.type) {
        case constants.SURNAME_INPUT_BLOCK.SET_SURNAME:
            return action.value;
        case constants.AVENGER_LIST.SET_CURRENT_AVENGER:
            return action.value.last_name;
        default:
            return state;
    }
};

export default surname;