import { combineReducers } from 'redux';
import surname from "./surname";
import isFocused from "./isFocused";

export default combineReducers({
    surname,
    isFocused,
});