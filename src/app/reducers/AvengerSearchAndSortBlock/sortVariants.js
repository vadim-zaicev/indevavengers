import { constants } from "../../actions/";

const defaultVariants = [
    {id: 1, title: "По возрасту", current: true, field: "birth_date"},
    {id: 2, title: "По имени", field: "first_name"},
    {id: 3, title: "По званию", field: "rank"},
];

const sortVariants = function(state = defaultVariants, action) {
    switch (action.type) {
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_SORT_VARIANTS:
            return action.value.slice();
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_SORTING_VARIANT:
            return state.map(v => {
                v.id === action.value.id
                    ? v.current = true
                    : v.current = false;
                return Object.assign({},v);
            });
        default:
            return state;
    }
};

export default sortVariants;