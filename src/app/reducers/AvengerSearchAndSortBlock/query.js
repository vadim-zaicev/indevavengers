import { constants } from "../../actions/";

const query = function(state = '', action) {
    switch (action.type) {
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_QUERY:
            return action.value;
        default:
            return state;
    }
};

export default query;