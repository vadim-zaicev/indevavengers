import { constants } from "../../actions/";

const isOpen = function(state = false, action) {
    switch (action.type) {
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_OPEN:
            return true;
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_CLOSE:
            return false;
        default:
            return state;
    }
};

export default isOpen;