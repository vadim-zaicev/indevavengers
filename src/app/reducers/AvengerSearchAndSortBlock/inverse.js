import { constants } from "../../actions/";

const inverse = function(state = false, action) {
    switch (action.type) {
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.TOGGLE_INVERSE:
            return !state;
        default:
            return state;
    }
};

export default inverse;