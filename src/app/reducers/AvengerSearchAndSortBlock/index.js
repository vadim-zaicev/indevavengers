import { combineReducers } from 'redux';
import isFocused from './isFocused';
import isOpen from './isOpen';
import query from './query';
import sortVariants from './sortVariants';
import inverse from './inverse';

export default combineReducers({
    isFocused,
    isOpen,
    query,
    sortVariants,
    inverse
});