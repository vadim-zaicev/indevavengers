import { constants } from "../../actions/";

const isFocused = function(state = false, action) {
    switch (action.type) {
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_FOCUSED:
            return true;
        case constants.AVENGER_SEARCH_AND_SORT_BLOCK.SET_UNFOCUSED:
            return false;
        default:
            return state;
    }
};

export default isFocused;