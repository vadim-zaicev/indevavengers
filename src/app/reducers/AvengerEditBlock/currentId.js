import { constants } from "../../actions/";

const currentId = function(state = 0, action) {
    switch (action.type) {
        case constants.AVENGER_LIST.SET_CURRENT_AVENGER:
            return action.value.id;
        default:
            return state;
    }
};

export default currentId;