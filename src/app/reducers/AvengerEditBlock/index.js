import { combineReducers } from 'redux';
import image from "./image";
import currentId from "./currentId";

export default combineReducers({
    image,
    currentId
});