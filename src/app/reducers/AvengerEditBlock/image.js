import { constants } from "../../actions/";

const image = function(state = '', action) {
    switch (action.type) {
        case constants.AVENGER_EDIT_BLOCK.SET_IMAGE:
            return action.value;
        case constants.AVENGER_LIST.SET_CURRENT_AVENGER:
            return action.value.image;
        default:
            return state;
    }
};

export default image;