import { constants } from "../../actions/";

const name = function(state = '', action) {
    switch (action.type) {
        case constants.NAME_INPUT_BLOCK.SET_NAME:
            return action.value;
        case constants.AVENGER_LIST.SET_CURRENT_AVENGER:
            return action.value.first_name;
        default:
            return state;
    }
};

export default name;