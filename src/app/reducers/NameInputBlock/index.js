import { combineReducers } from 'redux';
import name from "./name";
import isFocused from "./isFocused";

export default combineReducers({
    name,
    isFocused,
});