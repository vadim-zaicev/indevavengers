import { constants } from "../../actions/";

const rankList = function(state = [], action) {
    switch (action.type) {
        case constants.RANK_SELECT_BLOCK.SET_RANK_LIST:
            return action.value.slice();
        default:
            return state;
    }
};

export default rankList;