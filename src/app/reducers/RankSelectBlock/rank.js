import { constants } from "../../actions/";

const rank = function(state = {}, action) {
    switch (action.type) {
        case constants.RANK_SELECT_BLOCK.SET_RANK:
            return Object.assign({}, action.value);
        case constants.AVENGER_LIST.SET_CURRENT_AVENGER:
            return Object.assign({}, {id: ""+action.value.post});
        default:
            return state;
    }
};

export default rank;