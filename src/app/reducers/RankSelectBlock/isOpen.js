import { constants } from "../../actions/";

const isOpen = function(state = false, action) {
    switch (action.type) {
        case constants.RANK_SELECT_BLOCK.SET_OPEN:
            return true;
        case constants.RANK_SELECT_BLOCK.SET_CLOSE:
            return false;
        default:
            return state;
    }
};

export default isOpen;