import { combineReducers } from 'redux';

import isFocused from "./isFocused";
import isOpen from "./isOpen";
import rank from "./rank";
import rankList from "./rankList";

export default combineReducers({
    isFocused,
    isOpen,
    rank,
    rankList,
});