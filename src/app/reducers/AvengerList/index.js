import { combineReducers } from 'redux';

import avengerList from "./avengerList";

export default combineReducers({
    avengerList
});