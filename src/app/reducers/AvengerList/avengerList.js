import { constants } from "../../actions/";

const avengerList = function(state = [], action) {
    switch (action.type) {
        case constants.AVENGER_LIST.SET_LIST:
            return action.value.slice();

        case constants.AVENGER_EDIT_BUTTON_LINE.SAVE_CURRENT_AVENGER:

            const index = state.reduce((i, avenger, index) => {
                if (-1 != i) return i;
                if (avenger.id == action.value.id) return index;
                return -1;
            }, -1);

            return [...state.slice(0, index),
                Object.assign({}, state[index], action.value),
                ...state.slice(index+1)
            ];

        case constants.AVENGER_EDIT_BUTTON_LINE.DELETE_CURRENT_AVENGER:

            const index2 = state.reduce((i, avenger, index) => {
                if (-1 != i) return i;
                if (avenger.id == action.value.id) return index;
                return -1;
            }, -1);

            if (index2 === -1) return state;

            return [...state.slice(0, index2),
                ...state.slice(index2+1)
            ];

        default:
            return state;
    }
};

export default avengerList;