import { constants } from "../../actions/";

const spec = function(state = '', action) {
    switch (action.type) {
        case constants.AVENGER_SPEC_BLOCK.SET_SPEC:
            return action.value;
        case constants.AVENGER_LIST.SET_CURRENT_AVENGER:
            return action.value.description;
        default:
            return state;
    }
};

export default spec;