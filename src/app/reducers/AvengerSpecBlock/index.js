import { combineReducers } from 'redux';
import spec from './spec';
import isFocused from './isFocused';

export default combineReducers({
    spec,
    isFocused
});