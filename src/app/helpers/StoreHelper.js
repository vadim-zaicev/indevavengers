export class StoreHelper {
    static _store;

    static setStore(store) {
        StoreHelper._store = store;
    }

    static getStore() {
        return StoreHelper._store;
    }

    static getState() {
        return StoreHelper._store.getState();
    }
}